/* 
 * File:   Timer2.h
 * Author: Diango
 *
 * Created on 18 de abril de 2019, 10:37
 */

#ifndef TIMER2
#define	TIMER2

void ConfigTimer2();
void InitTimer(unsigned char PR2_Value);

#endif	/* TIMER2 */


/* 
 * File:   ADC.h
 * Author: Diango
 *
 * Created on 18 de abril de 2019, 10:24
 */

#ifndef ADC
#define	ADC

void ConfigADC();
unsigned int ReadADC();

#endif	/* ADC */


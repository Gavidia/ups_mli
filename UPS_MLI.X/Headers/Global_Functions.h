/* 
 * File:   Global_Functions.h
 * Author: Diango
 *
 * Created on 18 de abril de 2019, 9:26
 */
#include <p18cxxx.h>

#ifndef GLOBAL_FUNCTIONS
#define	GLOBAL_FUNCTIONS

void ConfigPorts();
void EnableInterrupts();

#endif	/* GLOBAL_FUNCTIONS */


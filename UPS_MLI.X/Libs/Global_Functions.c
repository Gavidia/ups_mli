#include <p18cxxx.h>
#include "../Headers/Global_Functions.h"

void EnableInterrupts(){
    INTCONbits.GIE = 1;//enable global interrupts
    INTCONbits.PEIE = 1; //enable peripherical interrupts
    INTCONbits.INT0IE =1; //enable INT0 Interrupt
}

void ConfigPorts(){
    TRISD = 0x00;
    PORTD = 0x00;
    ADCON1 = 0x0E; //AN0 Analógico, AN1-AN12 Digitales
}


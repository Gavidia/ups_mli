#include <p18cxxx.h>
#include "../Headers/Shift_Register.h"

unsigned char bitsMC = 8;
unsigned char shiftLeftSide = 8 - 1;
unsigned char shiftRightSide = 0;
unsigned char counterMC = 0;
unsigned char onBitMC = 0b0000001;
unsigned char onCycleMC = 0b0000001;

void ShiftRegister(){
    
    PORTD = PORTD ^ onBit;
    counterShift++;
    
    if(counterShift < 8){
        onBit = (onBit << 1) | (onBit >> (8 - 1));  
    }else if(counterShift >= 9){
        onBit = (onBit >> 1) | (onBit << (8 - 1));
        
        if(counterShift == 16){
            counterShift = 0;
            onBit = 1;
            cycle++;
            
            if(cycle == 8){
                cycle = 0;
            }
            
            onBit = (onBit << cycle);
        }
    }
    
    if(onBit == 0){
        onBit = 1;
    }
}

void ShiftRegisterMC(){
    
    if(counterMC != bitsMC){
        PORTD = PORTD ^ onBitMC;
    }
    if(counterMC == 0){
        onBitMC = onCycleMC;
    }
    
    counterMC++;  
    

    if(counterMC <= shiftLeftSide ||
       counterMC > bitsMC + 1 && counterMC <= bitsMC + shiftRightSide){
        
        onBitMC = onBitMC << 1;
        
    }else if(counterMC == shiftLeftSide + 1){
        
        onBitMC = onCycleMC >> 1;
        
    }else if(counterMC > shiftLeftSide + 1 && counterMC < bitsMC || 
             counterMC > bitsMC + shiftRightSide + 1 && counterMC < 16){
        
        onBitMC = onBitMC >> 1;
    }else if(counterMC == bitsMC + shiftRightSide + 1){
        
        onBitMC = 0b10000000;
    }
    
    if(counterMC == 16){
        
        if(onCycleMC == 0b10000000)
            onBitMC = 0b00000001;
        
        onBitMC = onBitMC | onCycleMC;
        onCycleMC = (onCycleMC << 1) | (onCycleMC >> (8 - 1));
        counterMC = 0;
        
        if(shiftRightSide == 7 && shiftLeftSide == 0){
            
            shiftRightSide = 0;
            shiftLeftSide = bitsMC - 1;
            
        }else{
            
            shiftLeftSide--;
            shiftRightSide++;  
            
        }
    }
    
}
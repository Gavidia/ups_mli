#include <p18cxxx.h>
#include "../Headers/Timer2.h"

void ConfigTimer2(){


	T2CON=0x02;
	IPR1bits.TMR2IP = 1; // high priority
	PIR1bits.TMR2IF = 0;
	PIE1bits.TMR2IE = 1; //1=enable Interrupt
}

void InitTimer2(unsigned char PR2_Value){

    TMR2 = 0;
	PR2 = PR2_Value; // T = (PR2+1) * 4 *(1/Fosc)*PRESCALER
	// 83.33us= (PR2+1)* 4*(1/Fosc)*PRESCALER : Fosc=48Mhz : PRESCALER=1:4
    T2CONbits.TMR2ON = 1;
}

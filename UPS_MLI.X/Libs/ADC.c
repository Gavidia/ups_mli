#include <p18cxxx.h>
#include "../Headers/ADC.h"

void ConfigADC(){
    
    TRISAbits.TRISA0 = 1;
    ADCON1bits.PCFG = 14; //Pin AN0 analógico AN1-AN12 Digitales
    ADCON1bits.VCFG = 0; //Voltaje de referencia VDD y VSS
    
    ADCON0 = 0; //Se selecciona el canal AN0
    
    ADCON2bits.ADFM = 1; //Justificación a la derecha
    ADCON2bits.ACQT = 5; //4TAD
    ADCON2bits.ADCS = 6; //FOSC/64
}

unsigned int ReadADC(){
    
    unsigned int result;
    ADCON0bits.ADON = 1; // ADC on
    ADCON0bits.GO_DONE = 1; // start
    while (ADCON0bits.GO_DONE == 1);
    result = ((unsigned int)(ADRESH) << 8) + ADRESL;
    ADCON0bits.ADON = 0; // ADC Off
    return result;
}
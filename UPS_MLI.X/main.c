/* 
 * File:   main.c
 * Author: Diango
 *
 * Created on 18 de abril de 2019, 7:39
 */

#include <stdio.h>

#include <stdlib.h>
#include <p18cxxx.h>


#pragma config PLLDIV = 5 // (20 MHz crystal)
#pragma config CPUDIV = OSC1_PLL2
#pragma config USBDIV = 2 // Clock source from 96MHz PLL/2
#pragma config FOSC = HSPLL_HS //HSPLL_HS
#pragma config WDT = OFF
/*
 * 
 */
unsigned int sine_vector_adc[100];
unsigned char counter = 0;
unsigned char PR2_Value = 62; // Tiempo = (PR2 + 1) * 4/FOSC * Prescaler * Postcaler
                              // Tiempo = (62) * 4/48000000 * 16 * 1 = 0.000083
#define GLOBAL_FUNCTIONS
#define TIMER2
#define ADC
#define SHIFT_REGISTER
//********************************************************************
// High priority interrupt routine
#pragma code
#pragma interrupt InterruptHandlerHigh

//*****************************************************************************/
void InterruptHandlerHigh ()
{
    // Es interrupción por cruce por cero?
    if (INTCONbits.INT0IF == 1){
        INTCONbits.INT0IF = 0;  
        counter = 0;
        InitTimer2(PR2_Value);
    }
    
    if(PIR1bits.TMR2IF == 1){
        PIR1bits.TMR2IF = 0;
        counter++;
        sine_vector_adc[counter] = ReadADC();
    }

}
void main() {
    
    ConfigPorts();
    EnableInterrupts();
    ConfigTimer2();
    ConfigADC();
    while(1){
        ShiftRegisterMC();
    };
    
}

// High priority interrupt vector
#pragma code InterruptVectorHigh = 0x08

void InterruptVectorHigh (void)
{
	_asm	
        goto InterruptHandlerHigh //jump to interrupt routine
	_endasm
}

